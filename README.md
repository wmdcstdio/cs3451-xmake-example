# CS3451 Computer Graphics Starter Code

## 0. Quick Guide

If you are already experienced with using CMake to generate a C++ project, please read the following paragraphs for a quick guide. If not, you may read Section 1 and 2 first and then get back to check this section as a summary.

### Code Structure

The starter codebase is organized as `ext`, `src`, `assignments` and `tutorials`. We put all external codes (e.g., the Eigen library) in `ext`. We put the common headers that can be shared among different subprojects in `src` (e.g., the classes of mesh, grid, file IO, etc.). The folder `assignments` maintains all assignments. An assignment is dependent on `src` and `ext`, but is independent from other assignments.

Usually, you are asked to write code in one or multiple files in an assignment folder. You don’t need to change files in ext or src.


## 1. Compile and run the first assignment:

### Step 1: Clone the source code from GitLab and enter the codebase folder

    git clone https://gitlab.com/boolzhu/cs3451-computer-graphics-starter-code/
    cd cs3451-computer-graphics-starter-code

### Step 2: Install xmake

Follow the instructions to install `xmake`:

https://xmake.io/#/getting_started


### Step 3: Compile and run the code

To test if everything works correctly you can run:

    xmake build a1
    xmake run a1

This will:

- Compile the code for the assignment 1 and any dependencies
- Popup a window and show an OpenGL window. You should see this image if everything works properly: ![a1image](misc/a1.JPG)  

### Step 4: Start implementing!

- Use your editor (`Visual Studio Code` recommended) to edit the assignment files in `assignments`!